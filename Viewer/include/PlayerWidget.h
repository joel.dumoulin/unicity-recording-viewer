//
// Created by Joël Dumoulin on 08.08.17.
//

#ifndef UNICITYRECORDING_PLAYERWIDGET_H
#define UNICITYRECORDING_PLAYERWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QSlider>
#include <QTimer>

class PlayerWidget : public QWidget {
Q_OBJECT

public:
    PlayerWidget();
    PlayerWidget(int nbFrames);

    void setNbFrames(int nbFrames);

    void setMinTimestamp(long timestamp);
    void setMaxTimestamp(long timestamp);

public slots:
    void btnPlayClicked();
    void btnStopClicked();
    void btnPauseClicked();
    void btnNextClicked();
    void btnPreviousClicked();
    void btnLastClicked();
    void btnFirstClicked();
    void sliderValueChanged(int);

    void timerUpdate();


signals:
    void goTo(long timestamp, bool playing);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    void initUi();

    QPushButton *btnPlay;
    QPushButton *btnStop;
    QPushButton *btnPause;
    QPushButton *btnNext;
    QPushButton *btnPrevious;
    QPushButton *btnLast;
    QPushButton *btnFirst;

    QLabel *lblFrame;

    QSlider *slider;

    int nbFrames;
    int crtIdx;

    void updateFrameLabel();
    void frameChanged();

    QTimer *timer;

    long minTimestamp = 0;
    long maxTimestamp = 0;
    long duration = 0;
    long crtTimestamp = 0;
    int frameDuration = 30;
    int frameFactor = 3;

    bool playing = false;
};


#endif //UNICITYRECORDING_PLAYERWIDGET_H
