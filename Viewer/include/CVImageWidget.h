//
// Created by Joël Dumoulin on 28.07.17.
//

#ifndef UNICITYRECORDING_CVIMAGEWIDGET_H
#define UNICITYRECORDING_CVIMAGEWIDGET_H

#include <QWidget>
#include <QImage>
#include <QPainter>

#include <QRadioButton>
#include <QLabel>

#include <string>

#include "boost/filesystem.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

class CVImageWidget : public QWidget {
    Q_OBJECT

public:
    CVImageWidget(std::string title, QWidget *parent = 0);

    QSize sizeHint() const;
    QSize minimumSizeHint() const;

    void setRgbChannel(std::vector<std::string> *channel);
    void setRgbHDChannel(std::vector<std::string> *channel);
    void setDepthChannel(std::vector<std::string> *channel);
    void setIrChannel(std::vector<std::string> *channel);

    //jules
    void setNumberOfSensors(int numberOfSensors);

public slots:
    void goTo(long timestamp, bool playing);
    void changeChannel();
    void showInfos(int val);

protected:
    void paintEvent(QPaintEvent* /*event*/);

    cv::Mat matread(const std::string& filename);

    void depthToPixel(cv::Mat& image);

    QImage _qimage;
    cv::Mat _tmp;

    void showImage();

    void updateInfos();

    std::string title;

    int crtIdx;

    // Channels
    std::vector<std::string> *crtChannel;
    std::vector<std::string> *rgbChannel;
    std::vector<std::string> *rgbHDChannel;
    std::vector<std::string> *depthChannel;
    std::vector<std::string> *irChannel;

    // Radio buttons
    QRadioButton *rbColor;
    QRadioButton *rbColorHD;
    QRadioButton *rbDepth;
    QRadioButton *rbIr;

    // Infos
    QLabel *lblCrtFrameNb;
    QLabel *lblCrtFrameTimestamp;
    QLabel *lblCrtFrameTimestampGlobal;
    QLabel *lblDepthStats;
    QLabel *lblFramerate;

    long maxTimestamp;
    long minTimestampGlobal;
    long maxTimestampGlobal;

    float crtFrameMin;
    float crtFrameMax;

    int crtFramerate;
    int minFramerate;
    int maxFramerate;
    float avgFramerate;

    bool infosVisible;

    bool needConversion;

    void getFrameTimestamps(boost::filesystem::path filepath, long &timestamp, long &timestampGlobal);

    void extractFramerate();

    //jules
    int numberOfSensors;
};

#endif //UNICITYRECORDING_CVIMAGEWIDGET_H
