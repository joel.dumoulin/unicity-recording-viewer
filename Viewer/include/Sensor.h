//
// Created by Joël Dumoulin on 27.07.17.
//

#ifndef UNICITYRECORDING_SENSOR_H
#define UNICITYRECORDING_SENSOR_H

#include "CVImageWidget.h"

#include <string>
#include <boost/filesystem.hpp>

#include <QWidget>

class Sensor : public QWidget{
    Q_OBJECT

public:
    Sensor(std::string id, std::string path);

    ~Sensor();

    enum ChannelType {color, color_hd, depth, ir};
    static ChannelType channelTypes;

    std::vector<std::string> colorFiles;
    std::vector<std::string> colorHdFiles;
    std::vector<std::string> depthFiles;
    std::vector<std::string> irFiles;

    const char* getId() const;
    const char* getPath() const;

    int getNbFrames();
    long getMinTimestamp();
    long getMaxTimestamp();

    bool isVisible();

    void setImage(CVImageWidget* image);
    CVImageWidget* getImage();



public slots:
    void on_sensorCheckBox_stateChanged(int state);

private:
    std::string id;
    boost::filesystem::path path;

    void prepare();

    void fillFiles(ChannelType channel, std::vector<std::string> *files);

    bool visible;

    boost::filesystem::path getChannelPath(ChannelType channelType) const;

    int nbFrames;
    long minTimestamp;
    long maxTimestamp;

    void getFrameTimestamps(boost::filesystem::path filepath, long &timestamp, long &timestampGlobal);

    CVImageWidget* cvImage;


};

#endif //UNICITYRECORDING_SENSOR_H
