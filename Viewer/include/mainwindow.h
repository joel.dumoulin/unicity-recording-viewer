#ifndef UNICITY_RECORDING_MAINWINDOW_H
#define UNICITY_RECORDING_MAINWINDOW_H

#include "Sensor.h"
#include "CVImageWidget.h"

#include "PlayerWidget.h"

#include <string>

#include <QMainWindow>
#include <QString>
#include <QStandardItemModel>
#include <QFileSystemModel>
#include <QTreeView>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QCheckBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


public slots:
    void selectBaseFolder();
    void recordingTreeViewClicked(const QModelIndex &index);
    void updateView();

private:
    Ui::MainWindow *ui;

    QString basePath;
    QLineEdit *basePathInput;
    QTreeView *recordingTreeView;

    QVBoxLayout *sensorsLayout;

    QGridLayout *viewGrid;

    QCheckBox *cbShowInfos;

    PlayerWidget *player;

    void updateRecordingModel();

    QFileSystemModel *recordingModel;

    std::string scenarioId;
    std::string recordingId;

    std::vector<Sensor*> sensors;

    void updateSensorsList();

    std::vector<CVImageWidget*> imageWidgets;


    void saveBasePath();
    void loadBasePath();

    const std::string settingsFile = "settings.yml";

    void align();
};

#endif // UNICITY_RECORDING_MAINWINDOW_H
