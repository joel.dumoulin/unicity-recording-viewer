#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <algorithm>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <QFileDialog>
#include <QCheckBox>
#include <QSpacerItem>
#include <QPushButton>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QSizePolicy>

#include <cmath>

#include <fstream>
#include "yaml-cpp/yaml.h"

#include <limits>
//
#include <QApplication>
#include <QDesktopWidget>
//

using namespace std;
using namespace boost::filesystem;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    recordingModel(new QFileSystemModel)
{
    ui->setupUi(this);

    //Availble geometry
    QRect rec = QApplication::desktop()->availableGeometry();
    int height=rec.height();
    int width=rec.width();
    //

    // Default base input
    this->loadBasePath();
    basePathInput = new QLineEdit;
    basePathInput->setText(basePath);

    QPushButton *selectBasePathButton = new QPushButton("...");
    QObject::connect(selectBasePathButton, SIGNAL(clicked()), this, SLOT(selectBaseFolder()));

    // Add components to basePathLayout
    QHBoxLayout *basePathLayout = new QHBoxLayout;
    basePathLayout->addWidget(basePathInput);
    basePathLayout->addWidget(selectBasePathButton);

    // Tree view
    recordingModel->setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    recordingTreeView = new QTreeView;
    recordingTreeView->setModel(recordingModel);
    recordingTreeView->hideColumn(1);
    recordingTreeView->hideColumn(2);
    recordingTreeView->hideColumn(3);
//    recordingTreeView->setMinimumHeight(height*0.4);
//    recordingTreeView->setMaximumHeight(height*0.4);
    recordingTreeView->setFixedHeight(height*0.35);
    recordingTreeView->setFixedWidth(width*0.18);
    QObject::connect(recordingTreeView, SIGNAL(clicked(const QModelIndex)), this, SLOT(recordingTreeViewClicked(const QModelIndex)));

    // Add components to recording layout
    QVBoxLayout *recordingLayout = new QVBoxLayout;
    recordingLayout->addLayout(basePathLayout);
    recordingLayout->addWidget(recordingTreeView);

    // Add Components to recordingGroupBox
    QGroupBox *recordingGroupBox = new QGroupBox("Recording session");
    recordingGroupBox->setLayout(recordingLayout);
    //recordingGroupBox->setMaximumWidth(0.2*width);
    recordingGroupBox->setFixedHeight(height*0.45);
    recordingGroupBox->setFixedWidth(width*0.2);

    // Add components to sensorsGroupBox
    sensorsLayout = new QVBoxLayout;
    sensorsLayout->addStretch(1);
    QGroupBox *sensorsGroupBox = new QGroupBox("Sensors");
    sensorsGroupBox->setLayout(sensorsLayout);
    //sensorsGroupBox->setMaximumWidth(0.2*width);
    sensorsGroupBox->setFixedHeight(height*0.3);
    sensorsGroupBox->setFixedWidth(width*0.2);

    // Infos group box
    QGroupBox *infosGroupBox = new QGroupBox("Infos");
    QVBoxLayout *infosLayout = new QVBoxLayout;

    cbShowInfos = new QCheckBox("Show infos");
    cbShowInfos->setChecked(true);
    infosLayout->addWidget(cbShowInfos);
    infosGroupBox->setLayout(infosLayout);
    //infosGroupBox->setMaximumWidth(0.2*width);
    infosGroupBox->setFixedHeight(height*0.15);
    infosGroupBox->setFixedWidth(width*0.2);

    // Create viewGrid
    viewGrid = new QGridLayout;
    viewGrid->setColumnStretch(0, 1);



    // Create player widget
    player = new PlayerWidget;

    // Add components to main grid
    QGridLayout *mainGrid = new QGridLayout;
    mainGrid->addWidget(recordingGroupBox, 0, 0, 4, 1);
    mainGrid->addWidget(sensorsGroupBox, 4, 0, 2, 1);
    mainGrid->addWidget(infosGroupBox, 6, 0, 1, 1);
    mainGrid->addLayout(viewGrid, 0, 1, 6, 4);
    mainGrid->addWidget(player, 6, 1, 1, 4);


    // Add main grid to center
    ui->centralwidget->setLayout(mainGrid);

    updateRecordingModel();
}

MainWindow::~MainWindow() {
    this->saveBasePath();
    delete ui;
    delete recordingModel;
}

void MainWindow::selectBaseFolder(){
    basePath = QFileDialog::getExistingDirectory(this);
    basePathInput->setText(basePath);
    updateRecordingModel();
}

void MainWindow::updateRecordingModel() {
    recordingModel->setRootPath(basePath);
    recordingTreeView->setRootIndex(recordingModel->index(basePath));
}

void MainWindow::recordingTreeViewClicked(const QModelIndex &index)
{
    // Get the full path of the item that's user clicked on
    std::string mPath = recordingModel->fileInfo(index).absoluteFilePath().toStdString();
    std::string mPathTemp = mPath;

    if(mPathTemp.substr(mPathTemp.size()-1, 1) == "/"){
        mPathTemp.erase(0, basePath.size());
    }else{
        mPathTemp.erase(0, basePath.size() + 1);
    }

    std::vector<std::string> parts;
    boost::split(parts, mPathTemp, boost::is_any_of("/"));

    if(parts.size() == 2){
        // Clear existing list of sensors
        sensors.clear();

        //jules
        imageWidgets.clear();

        scenarioId = parts[0];
        recordingId = parts[1];

        // List sensors (folders in mPath)
        directory_iterator end_itr;

        int maxNbFrames = 0;
        long minTimestamp = LONG_MAX;
        long maxTimestamp = 0;

        // Cycle through the directory
        for (directory_iterator itr(mPath); itr != end_itr; ++itr)
        {
            if (is_directory(itr->path())) {
                string path = itr->path().string();

                std::vector<std::string> pathParts;
                boost::split(pathParts, path, boost::is_any_of("/"));
                std::string id = pathParts.back();

                Sensor *newSensor = new Sensor{id, path};

                if(newSensor->getNbFrames() > 0){
                    // Update max nb of frames and min/max timestamps
                    maxNbFrames = std::max(maxNbFrames, newSensor->getNbFrames());
                    minTimestamp = std::min(minTimestamp, newSensor->getMinTimestamp());
                    maxTimestamp = std::max(maxTimestamp, newSensor->getMaxTimestamp());

                    // Create cvImageWidgets
                    CVImageWidget *cvImage = new CVImageWidget{newSensor->getId()};
                    //jules
                    imageWidgets.push_back(cvImage);
                    //
                    QObject::connect(player, SIGNAL(goTo(long, bool)), cvImage, SLOT(goTo(long, bool)));
                    QObject::connect(cbShowInfos, SIGNAL(stateChanged(int)), cvImage, SLOT(showInfos(int)));

                    cvImage->setRgbChannel(&(newSensor->colorFiles));
                    cvImage->setRgbHDChannel(&(newSensor->colorHdFiles));
                    cvImage->setDepthChannel(&(newSensor->depthFiles));
                    cvImage->setIrChannel(&(newSensor->irFiles));

                    newSensor->setImage(cvImage);
                    cvImage->sizePolicy().setHorizontalStretch(1);

                    sensors.push_back(newSensor);
                }
            }
        }

        if(maxNbFrames > 0){
            player->setNbFrames(maxNbFrames);

            player->setMinTimestamp(minTimestamp);
            player->setMaxTimestamp(maxTimestamp);

            //jules
            for(int i=0;i<imageWidgets.size();i++){
                imageWidgets.at(i)->setNumberOfSensors(sensors.size());
            }
            updateSensorsList();
            updateView();
        }
    }
}

void MainWindow::updateSensorsList(){
    // Remove existing widgets
    while (auto item = sensorsLayout->takeAt(0)) {
        delete item->widget();
        delete item;
    }

    // Remove existing widgets
    while (auto item = viewGrid->takeAt(0)) {
        delete item->widget();
        delete item;
    }

    // Add new checkboxes
    for(auto&& sensor : sensors){
        // Create a new checkbox and add it
        QCheckBox *sensorCheckBox = new QCheckBox(tr(sensor->getId()));
        sensorCheckBox->setChecked(true);
        sensorsLayout->addWidget(sensorCheckBox);

        // Connect checkbox signal to sensor slot
        QObject::connect(sensorCheckBox, SIGNAL(stateChanged(int)), sensor, SLOT(on_sensorCheckBox_stateChanged(int)));
        QObject::connect(sensorCheckBox, SIGNAL(stateChanged(int)), this, SLOT(updateView()));
    }

    // Add stretch (push components to the top)
    sensorsLayout->addStretch();

    // Init player
    player->goTo(0, false);


    // Init infos
    cbShowInfos->setChecked(true);
}

void MainWindow::updateView(){
    int nbVisible = 0;

    int idxRow = -1;
    int idxCol = -1;

    int mod;



    for(auto&& sensor : sensors){
        if(sensor->isVisible() == true){
            nbVisible++;
        }

        viewGrid->removeWidget(sensor->getImage());
        sensor->getImage()->hide();
    }

    mod = int(std::ceil(float(nbVisible) / float(2)));

    for(auto&& sensor : sensors){
        if(sensor->isVisible() == true){
            idxCol = (idxCol + 1) % mod;
            if(idxCol == 0){
                idxRow++;
            }

            viewGrid->addWidget(sensor->getImage(), idxRow, idxCol);
            sensor->getImage()->show();
        }
    }

}

void MainWindow::saveBasePath(){
    YAML::Emitter out;
    out << YAML::BeginMap;

    out << YAML::Key << "basePath";
    out << YAML::Value << basePath.toStdString();

    out << YAML::EndMap;


    std::ofstream fout(settingsFile);
    fout << out.c_str();
}

void MainWindow::loadBasePath(){
    try{
        YAML::Node config = YAML::LoadFile(settingsFile);

        if (config["basePath"]) {
            basePath = QString::fromStdString(config["basePath"].as<std::string>());
        }
    }catch(...){
        basePath = "";
    }
}