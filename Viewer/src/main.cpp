//
// Created by Joël Dumoulin on 26.07.17.
//
#include <iostream>
#include <string>

#include <QApplication>
#include <QPushButton>

#include "mainwindow.h"

using namespace std;

int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    MainWindow window;
    window.showMaximized();

    return app.exec();
}
