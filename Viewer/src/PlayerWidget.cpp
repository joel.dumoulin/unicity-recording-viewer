//
// Created by Joël Dumoulin on 08.08.17.
//

#include "PlayerWidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPixmap>
#include <QIcon>
//
#include <QApplication>
#include <QDesktopWidget>
//
#include <iostream>
#include <algorithm>

using namespace std;

PlayerWidget::PlayerWidget() : nbFrames(1) {
    initUi();
}

PlayerWidget::PlayerWidget(int nbFrames) : nbFrames(nbFrames) {
    initUi();
}

void PlayerWidget::initUi(){
    QVBoxLayout *vBox = new QVBoxLayout;
    QRect rec = QApplication::desktop()->availableGeometry();
    QSize btnSize=rec.size().scaled(rec.width()*0.02,rec.width()*0.02,Qt::IgnoreAspectRatio);
    //vBox->setSpacing(rec.width()*0.01);


    // CrtIdx / NbFrames labels
    lblFrame = new QLabel("0 / 0");
    lblFrame->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
    lblFrame->setContentsMargins(0,0,0,0);
    lblFrame->setSizePolicy(QSizePolicy::MinimumExpanding,
                         QSizePolicy::MinimumExpanding);

    vBox->addWidget(lblFrame);

    // Slider
    slider = new QSlider(Qt::Horizontal);
    QObject::connect(slider, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)));
    QObject::connect(slider, SIGNAL(sliderPressed()), this, SLOT(btnPauseClicked()));

    vBox->addWidget(slider);

    // Controls layout
    QHBoxLayout *controlsLayout = new QHBoxLayout;

    // First
    btnFirst = new QPushButton();
    QObject::connect(btnFirst, SIGNAL(clicked()), this, SLOT(btnFirstClicked()));

    QPixmap pixmapFirst(":/skip-backward.png");
    QIcon iconFirst(pixmapFirst);
    btnFirst->setIcon(iconFirst);
//    btnFirst->setIconSize(pixmapFirst.rect().size());
//    btnFirst->setFixedSize(pixmapFirst.rect().size());
    btnFirst->setIconSize(btnSize);
    btnFirst->setFixedSize(btnSize);

    // Backward
    btnPrevious = new QPushButton();
    QObject::connect(btnPrevious, SIGNAL(clicked()), this, SLOT(btnPreviousClicked()));

    QPixmap pixmapBackward(":/backward.png");
    QIcon iconBackward(pixmapBackward);
    btnPrevious->setIcon(iconBackward);
//    btnPrevious->setIconSize(pixmapBackward.rect().size());
//    btnPrevious->setFixedSize(pixmapBackward.rect().size());
    btnPrevious->setIconSize(btnSize);
    btnPrevious->setFixedSize(btnSize);

    // Play
    btnPlay = new QPushButton();
    QObject::connect(btnPlay, SIGNAL(clicked()), this, SLOT(btnPlayClicked()));

    QPixmap pixmapPlay(":/start.png");
    QIcon iconPlay(pixmapPlay);
    btnPlay->setIcon(iconPlay);
//    btnPlay->setIconSize(pixmapPlay.rect().size());
//    btnPlay->setFixedSize(pixmapPlay.rect().size());
    btnPlay->setIconSize(btnSize);
    btnPlay->setFixedSize(btnSize);


    // Stop
    btnStop = new QPushButton();
    QObject::connect(btnStop, SIGNAL(clicked()), this, SLOT(btnStopClicked()));

    QPixmap pixmapStop(":/stop.png");
    QIcon iconStop(pixmapStop);
    btnStop->setIcon(iconStop);
//    btnStop->setIconSize(pixmapStop.rect().size());
//    btnStop->setFixedSize(pixmapStop.rect().size());
    btnStop->setIconSize(btnSize);
    btnStop->setFixedSize(btnSize);

    // Pause
    btnPause = new QPushButton();
    QObject::connect(btnPause, SIGNAL(clicked()), this, SLOT(btnPauseClicked()));

    QPixmap pixmapPause(":/pause.png");
    QIcon iconPause(pixmapPause);
    btnPause->setIcon(iconPause);
//    btnPause->setIconSize(pixmapPause.rect().size());
//    btnPause->setFixedSize(pixmapPause.rect().size());
    btnPause->setIconSize(btnSize);
    btnPause->setFixedSize(btnSize);

    // Forward
    btnNext = new QPushButton();
    QObject::connect(btnNext, SIGNAL(clicked()), this, SLOT(btnNextClicked()));

    QPixmap pixmapForward(":/forward.png");
    QIcon iconForward(pixmapForward);
    btnNext->setIcon(iconForward);
//    btnNext->setIconSize(pixmapForward.rect().size());
//    btnNext->setFixedSize(pixmapForward.rect().size());
    btnNext->setIconSize(btnSize);
    btnNext->setFixedSize(btnSize);

    // Last
    btnLast= new QPushButton();
    QObject::connect(btnLast, SIGNAL(clicked()), this, SLOT(btnLastClicked()));

    QPixmap pixmapLast(":/skip-forward.png");
    QIcon iconLast(pixmapLast);
    btnLast->setIcon(iconLast);
//    btnLast->setIconSize(pixmapLast.rect().size());
//    btnLast->setFixedSize(pixmapLast.rect().size());
    btnLast->setIconSize(btnSize);
    btnLast->setFixedSize(btnSize);

    // Add buttons
    controlsLayout->addStretch(5);
    controlsLayout->addWidget(btnFirst);
    controlsLayout->addStretch(1);
    controlsLayout->addWidget(btnPrevious);
    controlsLayout->addWidget(btnPlay);
    controlsLayout->addWidget(btnPause);
    controlsLayout->addWidget(btnNext);
    controlsLayout->addStretch(1);
    controlsLayout->addWidget(btnLast);
    controlsLayout->addStretch(5);

    btnPause->hide();

    vBox->addLayout(controlsLayout);

    this->setLayout(vBox);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
}

void PlayerWidget::setNbFrames(int nbFrames){
    this->nbFrames = nbFrames;
    this->crtIdx = 0;

    slider->setMaximum(nbFrames-1);
    slider->setValue(0);

    updateFrameLabel();
}

void PlayerWidget::setMinTimestamp(long timestamp){
    this->minTimestamp = timestamp;
}

void PlayerWidget::setMaxTimestamp(long timestamp){
    this->maxTimestamp = timestamp;
    this->duration = this->maxTimestamp - this->minTimestamp;

    slider->setMaximum((int)this->duration);
    slider->setValue(0);

    updateFrameLabel();
}

void PlayerWidget::timerUpdate(){
//    if(crtIdx < (nbFrames - 1)){
//        crtIdx++;
//    }else{
//        btnPauseClicked();
//    }

    crtTimestamp = std::min(crtTimestamp + (frameDuration * frameFactor), duration);

    if(crtTimestamp == duration){
        btnPauseClicked();
    }

    frameChanged();
}

void PlayerWidget::btnPlayClicked(){
    playing = true;
    btnPlay->hide();
    btnPause->show();

    timer->start(33*frameFactor);
}

void PlayerWidget::btnStopClicked(){
}

void PlayerWidget::btnPauseClicked(){
    playing = false;
    btnPlay->show();
    btnPause->hide();
    timer->stop();
}

void PlayerWidget::btnNextClicked(){
    btnPauseClicked();

//    if(crtIdx < (nbFrames - 1)){
//        crtIdx++;
//    }

    crtTimestamp = std::min(crtTimestamp + frameDuration, duration);

    frameChanged();
}

void PlayerWidget::btnPreviousClicked(){
    btnPauseClicked();

//    if(crtIdx > 0){
//        crtIdx--;
//    }

    crtTimestamp = std::max((int)(crtTimestamp - frameDuration), 0);

    frameChanged();
}

void PlayerWidget::btnLastClicked(){
    btnPauseClicked();

    //crtIdx = nbFrames - 1;
    crtTimestamp = duration;

    frameChanged();
}

void PlayerWidget::btnFirstClicked(){
    btnPauseClicked();

//    crtIdx = 0;
    crtTimestamp = 0;
    frameChanged();
}

void PlayerWidget::sliderValueChanged(int value){
//    crtIdx = value;
    crtTimestamp = (long)value;

    emit goTo(minTimestamp + crtTimestamp, playing);
    updateFrameLabel();
}

void PlayerWidget::frameChanged(){
    emit goTo(minTimestamp + crtTimestamp, playing);
    updateFrameLabel();

    slider->setValue(crtTimestamp);
}

void PlayerWidget::keyPressEvent(QKeyEvent *event) {
    std::cout << "Key pressed" << std::endl;
}

void PlayerWidget::updateFrameLabel() {
    std::string lbl = std::to_string(crtTimestamp) + " / " + std::to_string(duration);
    lblFrame->setText(QString::fromStdString(lbl));
}

