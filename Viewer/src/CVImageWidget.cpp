//
// Created by Joël Dumoulin on 28.07.17.
//

#include <CVImageWidget.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
//
#include <QApplication>
#include <QDesktopWidget>
//

#include <iomanip>
#include <sstream>
#include <algorithm>
#include <climits>

#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost::filesystem;
using namespace cv;

CVImageWidget::CVImageWidget(std::string title, QWidget *parent) : title(title), infosVisible(true),
                                                                   needConversion(true), maxTimestamp(0),
                                                                   minTimestampGlobal(0), maxTimestampGlobal(0),
                                                                   crtFrameMin(0), crtFrameMax(0), crtFramerate(0),
                                                                   minFramerate(INT_MAX), maxFramerate(0), avgFramerate(0),
                                                                   QWidget(parent) {
    QVBoxLayout *vbox = new QVBoxLayout;
    QHBoxLayout *hbox = new QHBoxLayout;

    QLabel *lblTitle = new QLabel(QString::fromStdString(title));
    lblTitle->setStyleSheet("QLabel { color : #00FF00; }");

    hbox->addWidget(lblTitle);

    // Radio buttons
    rbColor = new QRadioButton(tr("Color"));
    rbColorHD = new QRadioButton(tr("Color HD"));
    rbDepth = new QRadioButton(tr("Depth"));
    rbIr = new QRadioButton(tr("IR"));

    rbDepth->setChecked(true);

    rbColor->setStyleSheet("QRadioButton { color : #00FF00; }");
    rbColorHD->setStyleSheet("QRadioButton { color : #00FF00; }");
    rbDepth->setStyleSheet("QRadioButton { color : #00FF00; }");
    rbIr->setStyleSheet("QRadioButton { color : #00FF00; }");

    QObject::connect(rbColor, SIGNAL(clicked()), this, SLOT(changeChannel()));
    QObject::connect(rbColorHD, SIGNAL(clicked()), this, SLOT(changeChannel()));
    QObject::connect(rbDepth, SIGNAL(clicked()), this, SLOT(changeChannel()));
    QObject::connect(rbIr, SIGNAL(clicked()), this, SLOT(changeChannel()));

    hbox->addStretch(1);
    hbox->addWidget(rbDepth);
    hbox->addWidget(rbIr);
    hbox->addWidget(rbColor);
    hbox->addWidget(rbColorHD);

    vbox->addLayout(hbox);

    // Infos
    lblCrtFrameNb = new QLabel();
    lblCrtFrameNb->setStyleSheet("QLabel { color : #00FF00; }");

    lblCrtFrameTimestamp = new QLabel();
    lblCrtFrameTimestamp->setStyleSheet("QLabel { color : #00FF00; }");

    lblCrtFrameTimestampGlobal = new QLabel();
    lblCrtFrameTimestampGlobal->setStyleSheet("QLabel { color : #00FF00; }");

    lblFramerate = new QLabel();
    lblFramerate->setStyleSheet("QLabel { color : #00FF00; }");

    lblDepthStats = new QLabel();
    lblDepthStats->setStyleSheet("QLabel { color : #00FF00; }");

    vbox->addWidget(lblCrtFrameNb);
    vbox->addWidget(lblCrtFrameTimestamp);
    vbox->addWidget(lblCrtFrameTimestampGlobal);
    vbox->addWidget(lblFramerate);
    vbox->addWidget(lblDepthStats);

    vbox->addStretch(1);

    this->setLayout(vbox);
}

QSize CVImageWidget::sizeHint() const { return _qimage.size(); }

QSize CVImageWidget::minimumSizeHint() const { return _qimage.size(); }

void CVImageWidget::showImage() {
    std::string imagePath;

    try{
        imagePath = (*crtChannel)[crtIdx];

        Mat image;


        if (needConversion == true) {
            image = matread(imagePath);
            if(!image.empty()){
                depthToPixel(image);
            }
        } else {
            image = imread(imagePath, CV_LOAD_IMAGE_COLOR);

            if (!image.empty() && rbColorHD->isChecked() == true) {
                // Resize image because too big
                cv::Size size(524, 295);
                cv::resize(image, image, size);
            }
        }

        if(image.empty()) return;

        // Convert the image to the RGB888 format
        switch (image.type()) {
            case CV_8UC1:
                cvtColor(image, _tmp, CV_GRAY2RGB);
                break;
            case CV_8UC3:
                cvtColor(image, _tmp, CV_BGR2RGB);
                break;
        }
    }catch(...){
        std::cout << "Exception while rendering image... : " << imagePath << std::endl;
        return;
    }

    // QImage needs the data to be stored continuously in memory
    assert(_tmp.isContinuous());
    // Assign OpenCV's image buffer to the QImage. Note that the bytesPerLine parameter
    // (http://qt-project.org/doc/qt-4.8/qimage.html#QImage-6) is 3*width because each pixel
    // has three bytes.
    _qimage = QImage(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols * 3, QImage::Format_RGB888);


    //
    QRect rec = QApplication::desktop()->availableGeometry();
    int height=0.70*rec.height();
    int width=0.75*rec.width();
    int wmod = int(std::ceil(float(numberOfSensors) / float(2)));
    int hmod = int(std::ceil(float(numberOfSensors)/float(wmod)));

    _qimage = QImage(_qimage.scaled(width/(wmod+0.01),height/(hmod+0.01),Qt::KeepAspectRatio));
    this->setFixedSize(_qimage.width(),_qimage.height());

    //this->setFixedSize(image.cols, image.rows);

    repaint();
}

void CVImageWidget::paintEvent(QPaintEvent * /*event*/) {
    // Display the image
    QPainter painter(this);
    painter.drawImage(QPoint(0, 0), _qimage);
    painter.end();
}

Mat CVImageWidget::matread(const string &filename) {
    std::ifstream fs(filename, std::fstream::binary);

    // Header
    int rows, cols, type, channels;
    fs.read((char *) &rows, sizeof(int));         // rows
    fs.read((char *) &cols, sizeof(int));         // cols
    fs.read((char *) &type, sizeof(int));         // type
    fs.read((char *) &channels, sizeof(int));     // channels

    // Data
    cv::Mat mat(rows, cols, type);
    fs.read((char *) mat.data, CV_ELEM_SIZE(type) * rows * cols);

    return mat;
}

void CVImageWidget::depthToPixel(Mat &image) {
    double min, max;

    minMaxLoc(image, &min, &max);

    crtFrameMin = (float) min;
    crtFrameMax = (float) max;

    if (crtChannel == depthChannel) {
        if (max > 60000.) { // Deal with Fotonic range
            max = 3000; // Max is 3m

            Mat mask = image > max;
            image.setTo(max, mask);
        }
    }

    image.convertTo(image, CV_8U, 255.0 / (max - min), -min * 255.0 / (max - min));
}

void CVImageWidget::goTo(long timestamp, bool playing){
    if(timestamp >= maxTimestampGlobal){
        crtIdx = crtChannel->size() - 1;
    }else if(timestamp <= minTimestampGlobal){
        crtIdx = 0;
    }else{
        if(!playing){
            crtIdx = 0;
        }

        long _temp;
        long crtTimestamp;
        this->getFrameTimestamps(crtChannel->at(crtIdx), _temp, crtTimestamp);
        while(crtTimestamp < timestamp){
            crtIdx++;
            this->getFrameTimestamps(crtChannel->at(crtIdx), _temp, crtTimestamp);
        }
    }

    showImage();

    updateInfos();
}

void CVImageWidget::setRgbChannel(std::vector<std::string> *channel) {
    rgbChannel = channel;

    if (rgbChannel->size() == 0) {
        rbColor->hide();
    }
}

void CVImageWidget::setRgbHDChannel(std::vector<std::string> *channel) {
    rgbHDChannel = channel;

    if (rgbHDChannel->size() == 0) {
        rbColorHD->hide();
    }
}

void CVImageWidget::setDepthChannel(std::vector<std::string> *channel) {
    depthChannel = channel;
    crtChannel = depthChannel;

    if (depthChannel->size() == 0) {
        rbDepth->hide();
    } else {
        extractFramerate();
    }
}

void CVImageWidget::setIrChannel(std::vector<std::string> *channel) {
    irChannel = channel;

    if (irChannel->size() == 0) {
        rbIr->hide();
    }
}

void CVImageWidget::changeChannel() {
    if (rbColor->isChecked()) {
        crtChannel = rgbChannel;
        needConversion = false;
    } else if (rbColorHD->isChecked()) {
        crtChannel = rgbHDChannel;
        needConversion = false;
    } else if (rbDepth->isChecked()) {
        crtChannel = depthChannel;
        needConversion = true;
    } else if (rbIr->isChecked()) {
        crtChannel = irChannel;
        needConversion = true;
    }

    showImage();
    updateInfos();
}

void CVImageWidget::showInfos(int val) {
    if (val == 0) {
        infosVisible = false;

        lblCrtFrameNb->hide();
        lblCrtFrameTimestamp->hide();
        lblCrtFrameTimestampGlobal->hide();
        lblFramerate->hide();
        lblDepthStats->hide();
    } else {
        infosVisible = true;

        lblCrtFrameNb->show();
        lblCrtFrameTimestamp->show();
        lblCrtFrameTimestampGlobal->show();
        lblFramerate->show();
        lblDepthStats->show();

        updateInfos();
    }
}

void CVImageWidget::updateInfos() {
    stringstream stream;

    boost::filesystem::path framePath((*crtChannel)[crtIdx]);

    // Crt frame nb
    lblCrtFrameNb->setText(QString::fromStdString(
            "Frame: " + std::to_string(crtIdx + 1) + " / " + std::to_string(crtChannel->size())));

    // Crt frame timestamp
    long crtTimestamp, crtTimestampGlobal;
    getFrameTimestamps(framePath, crtTimestamp, crtTimestampGlobal);

    lblCrtFrameTimestamp->setText(QString::fromStdString(
            "Timestamp: " + std::to_string(crtTimestamp) + " / " + std::to_string(maxTimestamp)));
    lblCrtFrameTimestampGlobal->setText(QString::fromStdString(
            "Timestamp global: " + std::to_string(crtTimestampGlobal) + " / " +
            std::to_string(maxTimestampGlobal)));


    // Framerate
    if(crtIdx > 0){
        boost::filesystem::path previousFramePath((*crtChannel)[crtIdx-1]);
        long previousTimestamp, previousTimestampGlobal;
        getFrameTimestamps(previousFramePath, previousTimestamp, previousTimestampGlobal);

        int denominator = (int)crtTimestamp - (int)previousTimestamp;

        if(denominator == 0){
            crtFramerate = 0;
        }else{
            crtFramerate = 1000 / denominator;
        }
    }else{
        crtFramerate = 0;
    }

    stream << std::fixed << std::setprecision(2) << avgFramerate;
    string strAvgFramerate = stream.str();
    lblFramerate->setText(QString::fromStdString(
            "Framerate: crt = " + std::to_string(crtFramerate) + " (min = " + std::to_string(minFramerate) +
            " ; max = " + std::to_string(maxFramerate) + " ; avg = " + strAvgFramerate) + ")");

    // Depth stats
    if (crtChannel == depthChannel) {
        QString strDepthStats;
        stream.str("");
        stream << std::fixed << std::setprecision(2) << crtFrameMin;
        string strCrtFrameMin = stream.str();

        stream.str("");
        stream << std::fixed << std::setprecision(2) << crtFrameMax;
        string strCrtFrameMax = stream.str();

        strDepthStats = QString::fromStdString(
                "Depth distance: Min = " + strCrtFrameMin + " ; Max = " + strCrtFrameMax);

        lblDepthStats->setText(strDepthStats);
        if(infosVisible == true){
            lblDepthStats->show();
        }
    } else {
        lblDepthStats->hide();
    }
}

void CVImageWidget::getFrameTimestamps(boost::filesystem::path filepath, long &timestamp, long &timestampGlobal){
    std::vector<std::string> parts;
    boost::split(parts, filepath.filename().string(), boost::is_any_of("_"));

    try{
        timestamp = std::stol(parts[1]);
        timestampGlobal = std::stol(parts[0]);
    }catch(...){
        std::cerr << "Unable to convert string to long...";
    }
}

void CVImageWidget::extractFramerate() {
    // Get min/max timestamps
    boost::filesystem::path firstFramePath(depthChannel->front());
    boost::filesystem::path lastFramePath(depthChannel->back());

    long minTimestamp;
    getFrameTimestamps(firstFramePath, minTimestamp, minTimestampGlobal);
    getFrameTimestamps(lastFramePath, maxTimestamp, maxTimestampGlobal);

    long timestamp, timestampGlobal;
    int previousTimestamp = 0;
    int framerate = 0;
    long avgFramerateAcc = 0;

    int denominator;

    // Find min/max/avg framerate
    for(auto const& strFilepath: *depthChannel){
        boost::filesystem::path framepath(strFilepath);
        getFrameTimestamps(framepath, timestamp, timestampGlobal);

        if(previousTimestamp > 0){
            denominator = (int)timestamp - previousTimestamp;
            if(denominator == 0){
                framerate = 0;
            }else{
                framerate = 1000 / denominator;
            }

            minFramerate = std::min(minFramerate, framerate);
            maxFramerate = std::max(maxFramerate, framerate);
            avgFramerateAcc += framerate;
        }

        previousTimestamp = timestamp;
    }

    avgFramerate = (float)avgFramerateAcc / (float)(depthChannel->size()-1);
}


//jules
void CVImageWidget::setNumberOfSensors(int numberOfSensors) {
    this->numberOfSensors=numberOfSensors;
}
