//
// Created by Joël Dumoulin on 14.07.17.
//

#include <iostream>

#include <string>
#include "boost/filesystem.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace boost::filesystem;
using namespace cv;

#ifdef LINUX
    const int KEY_ESC = 27;
    const int KEY_LEFT = 81;
    const int KEY_RIGHT = 83;
#elif defined MACOS
    const int KEY_ESC = 27;
    const int KEY_LEFT = 2;
    const int KEY_RIGHT = 3;
#else
    #error "No platform defined"
#endif

Mat matread(const string& filename)
{
    std::ifstream fs(filename, std::fstream::binary);

    // Header
    int rows, cols, type, channels;
    fs.read((char*)&rows, sizeof(int));         // rows
    fs.read((char*)&cols, sizeof(int));         // cols
    fs.read((char*)&type, sizeof(int));         // type
    fs.read((char*)&channels, sizeof(int));     // channels

    // Data
    cv::Mat mat(rows, cols, type);
    fs.read((char*)mat.data, CV_ELEM_SIZE(type) * rows * cols);

    return mat;
}

void depthToPixel(Mat& image){
    double min, max;

    minMaxLoc(image, &min, &max);

//    std::cout << "Min: " << to_string(min) << ", Max: " << max << std::endl;

    image.convertTo(image, CV_8U, 255.0/(max - min), -min * 255.0/(max - min));
}

int main(int argc, char *argv[]) {
    path folderPath;

    if( argc == 2 ) {
        folderPath = path(argv[1]);
    } else {
        std::cout << "Two argument expected. " << std::endl;

//        folderPath = path("/Users/joel/dev/projects/unicity/out/01/002/Kinect01/depth");
        folderPath = path("/home/joel/dev/out/01/003/Kinect01/depth");
    }

    directory_iterator end_itr;

    vector<string> files;

    // cycle through the directory
    for (directory_iterator itr(folderPath); itr != end_itr; ++itr)
    {
        // If it's not a directory, list it. If you want to list directories too, just remove this check.
        if (is_regular_file(itr->path())) {
            // assign current file name to current_file and echo it out to the console.
            string current_file = itr->path().string();
            files.push_back(current_file);
//            cout << current_file << endl;
        }
    }

    // Sort by filename
    std::sort (files.begin(), files.end());

    int size = files.size();

    Mat image;
    int idx = 0;
    int keyPressed = 0;

    while(keyPressed != KEY_ESC){
        image = matread(files[idx]);

        depthToPixel(image);
        namedWindow("Viewer", WINDOW_AUTOSIZE); // Create a window for display.
        imshow("Viewer", image); // Show our image inside it.

        keyPressed = waitKey(0); // Wait for a keystroke in the window

        if(keyPressed == KEY_RIGHT){
            if(idx < (size - 1)){
                idx++;
            }
        }else if(keyPressed == KEY_LEFT){
            if(idx > 0){
                idx--;
            }
        }
    }
}