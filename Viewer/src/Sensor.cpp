//
// Created by Joël Dumoulin on 27.07.17.
//

#include "Sensor.h"
#include <iostream>

#include <algorithm>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost::filesystem;

Sensor::Sensor(std::string id, std::string path) : id(id), path(path), colorFiles(), colorHdFiles(), depthFiles(), irFiles(), visible(true), nbFrames(0) {
    prepare();
}

Sensor::~Sensor() {}

const char* Sensor::getId() const {
    return id.c_str();
}

const char* Sensor::getPath() const {
    return path.c_str();
}

boost::filesystem::path Sensor::getChannelPath(ChannelType channelType) const {
    switch (channelType)
    {
        case color:
            return path / "color";
        case color_hd:
            return path / "color_hd";
        case depth:
            return path / "depth";
        case ir:
            return path / "ir";
        default:
            return "";
    }
}

void Sensor::prepare(){
    fillFiles(ChannelType::color, &colorFiles);
    fillFiles(ChannelType::color_hd, &colorHdFiles);
    fillFiles(ChannelType::depth, &depthFiles);
    fillFiles(ChannelType::ir, &irFiles);
}

void Sensor::fillFiles(ChannelType channel, std::vector<std::string> *files){
    boost::filesystem::path path = getChannelPath(channel);

    // Find files and fill vector
    // List sensors (folders in mPath)
    directory_iterator end_itr;

    // Cycle through the directory
    for (directory_iterator itr(path); itr != end_itr; ++itr)
    {
        if (is_regular_file(itr->path())) {
            files->push_back(itr->path().string());
        }
    }

    // Sort by filename
    std::sort(files->begin(), files->end());

    if(files->size() > 0 && nbFrames == 0){
        nbFrames = files->size();
        long timestamp;
        boost::filesystem::path firstFramePath(files->front());
        boost::filesystem::path lastFramePath(files->back());
        this->getFrameTimestamps(firstFramePath, timestamp, minTimestamp);
        this->getFrameTimestamps(lastFramePath, timestamp, maxTimestamp);
    }
}

void Sensor::getFrameTimestamps(boost::filesystem::path filepath, long &timestamp, long &timestampGlobal){
    std::vector<std::string> parts;
    boost::split(parts, filepath.filename().string(), boost::is_any_of("_"));

    try{
        timestamp = std::stol(parts[1]);
        timestampGlobal = std::stol(parts[0]);
    }catch(...){
        std::cerr << "Unable to convert string to long...";
    }
}

void Sensor::on_sensorCheckBox_stateChanged(int state) {
    if(state == 0){
        visible = false;
    }else{
        visible = true;
    }
//    std::cout << "Visibility: " << visible << std::endl;
}

int Sensor::getNbFrames() {
    return nbFrames;
}

long Sensor::getMinTimestamp() {
    return minTimestamp;
}

long Sensor::getMaxTimestamp() {
    return maxTimestamp;
}

bool Sensor::isVisible(){
    return visible;
}

void Sensor::setImage(CVImageWidget* image){
    cvImage = image;

}

CVImageWidget* Sensor::getImage(){
    return cvImage;
}
