FROM pathtrk/docker-python3-opencv:contrib

RUN apt-get update && \
    apt-get install -y \
      build-essential \
      python-dev \
      python3-dev \
      locales \
      libblas-dev \
      liblapack-dev \
      libatlas-base-dev \
      autotools-dev \
      libicu-dev \
      libbz2-dev \
      gfortran

# install gcc and g++ so that liblpclassifier_cv32 can utilize the library
RUN echo 'deb http://deb.debian.org/debian/ sid main' >> /etc/apt/sources.list
RUN apt-get update -y && \
    apt-get install -y \
      gcc-5 \
      g++-5 && \
    rm -rf /var/lib/apt/lists/*

# install cmake
RUN apt-get remove -y cmake
RUN curl -O https://cmake.org/files/v3.8/cmake-3.8.2-Linux-x86_64.sh
RUN sh cmake-3.8.2-Linux-x86_64.sh --skip-license

# install boost with -fPIC
RUN wget https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.tar.gz
RUN tar xvzf boost_1_64_0.tar.gz
WORKDIR boost_1_64_0/
RUN ./bootstrap.sh --prefix=/usr/local
RUN ./b2 -j $(nproc) cxxflags=-fPIC install; exit 0
WORKDIR /
RUN rm -rf /boost_1_64_0**
RUN echo "/usr/local/lib" >> /etc/ld.so.conf.d/local.conf
RUN ldconfig

# install viewer dependencies
RUN apt-get update && \
    apt-get install -y \
      qtbase5-dev

RUN git clone https://github.com/jbeder/yaml-cpp.git
RUN cd yaml-cpp; \
    mkdir build; \
    cd build; \
    cmake -DBUILD_SHARED_LIBS=ON .. ; \
    make; \
    make install

# Add ssh
RUN mkdir /root/.ssh/
ADD id_rsa /root/.ssh/id_rsa

# Create known_hosts
RUN touch /root/.ssh/known_hosts; \
    ssh-keyscan gitlab.forge.hefr.ch >> /root/.ssh/known_hosts; \
    chmod 400 /root/.ssh/id_rsa; \
    mkdir /usr/src/app; \
    cd /usr/src/app; \
    git clone git@gitlab.forge.hefr.ch:joel.dumoulin/unicity-recording-viewer.git -b master;

WORKDIR /usr/src/app

RUN cd unicity-recording-viewer; \
    mkdir build; \
    cd build; \
    cmake -DCMAKE_BUILD_TYPE=Release -DLINUX=ON -lyaml-cpp ../; \
    make

VOLUME /usr/src/app

