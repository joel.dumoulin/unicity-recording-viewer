# Usage

- Start by clicking on the button with "..." and open the folder containing the recordings.
- In the tree view, select the recording you want to visualize (the folders XXX inside the scenarios folder, for instance 001)
- Once loaded, you can use the viewer interface (show/hide sensors, show/hide information, play/pause, frame by frame)


# Use with docker (unusable on MacOs, not tested on Ubuntu!)

## Build image

```
docker build -t viewer .
```

## Run app

```
docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v ${absolute_path_to_data}:/usr/src/app/data -it viewer /usr/src/app/unicity-recording-viewer/build/Viewer/src/Viewer
```

# Installation

## Install on Linux

### OpenCV 3.2.0

* http://docs.opencv.org/3.2.0/d7/d9f/tutorial_linux_install.html

If not finding libopencv_core.so.2.4:

Make symbolic links in /usr/local/lib and then run program with:

```
sudo ln -s libopencv_core.so.3.2 libopencv_core.so.2.4
sudo ln -s libopencv_imgproc.so.3.2 libopencv_imgproc.so.2.4
sudo -E LD_LIBRARY_PATH=/usr/local/lib ./TestRecorder
```

### Boost 1.64

Download and extract:

```
wget -O boost_1_64_0.tar.gz http://sourceforge.net/projects/boost/files/boost/1.64.0/boost_1_64_0.tar.gz/download
tar xzvf boost_1_64_0.tar.gz
cd boost_1_64_0/
```

Install required libraries:

```
sudo apt-get update
sudo apt-get install build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev
```

Prepare, build and install:

```
./bootstrap.sh --prefix=/usr/local
./b2
sudo ./b2 install
```

### Qt 5.x

```
sudo apt-get install qtdeclarative5-dev
```

### YAML-CPP

```
sudo apt-get install libyaml-cpp-dev
```

## Install on MacOS

### OpenCV 3

```
 brew install opencv3
```

### Boost 1.64

```
brew install boost
```

### Qt 5.x

```
brew install qt
```

or download and install from website : https://www1.qt.io/download-open-source/

### YAML-CPP

```
brew install yaml-cpp
```

# Compile and run
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DLINUX=ON -lyaml-cpp ../
make
```

(Replace -DLINUX=ON by -DMACOS=ON if compiling on Mac OS)

To run it:

```
cd build/Viewer/src
./Viewer
```

# If using CLion to compile

In *Preferences/Build, Execution, Deployment/CMake* :

CMake options: -DLINUX=ON -DCMAKE_PREFIX_PATH=${Qt cmake path}

(Replace -DLINUX=ON by -DMACOS=ON if compiling on Mac OS)

For instance on mac: ${Qt cmake path} = /Users/joel/dev/libs/**Qt/5.9/clang_64/lib/cmake**
